#include "admincard.h"
#include "ui_admincard.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newgroup.h"
#include "gruopwindow.h"
#include "loginserver.h"
#include "stuff/NetworkManager.h"
#include "timetable.h"
#include "studentnotes.h"
#include "admintable.h"
#include "review.h"
#include <QMessageBox>

AdminCard::AdminCard(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AdminCard)
{
    ui->setupUi(this);
    setWindowTitle("Назначить права");
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM teacher"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem1=new QListWidgetItem();
            QString name = row.get("teacher_name").asString();
            QString suname = row.get("teacher_suname").asString();
            QString patronymic = row.get("teacher_patronymic").asString();
            ListItem1->setText(suname+ " "+ name + " "+ patronymic);
            ListItem1->setData(Qt::UserRole,row.get("rowid").asInt());


            ui->listWidget_2->addItem(ListItem1);
        }

    });
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM student "}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString name = row.get("student_name").asString();
            QString suname = row.get("student_suname").asString();
            QString patronymic = row.get("student_patronymic").asString();
            ListItem->setText(suname+ " "+ name + " "+ patronymic);
            ListItem->setData(Qt::UserRole,row.get("rowid").asInt());


            ui->listWidget->addItem(ListItem);
        }

    });


}

AdminCard::~AdminCard()
{
    delete ui;

}

void AdminCard::on_pushButton_clicked()
{
    QString ad = ui->comboBox->currentText();
    QString user_role_id;
    if(ad == "Методист"){
        user_role_id = "1";
    }
    if(ad == "Преподаватель практики"){
        user_role_id = "2";
    }
    if(ad == "Администратор кафедры"){
        user_role_id = "3";
    }
    if(ad == "Студент"){
        user_role_id = "4";
    }

    NetworkManager::send(Request(SQL_OPERATOR, "", {
        {"sql_operator", "INSERT INTO user_registered (user_name,user_role_id ) VALUES ( '"
          + ui->lineEdit->text() + "'," + user_role_id +")"
         " RETURNING rowid"
        }

    }), [=](const Response& resp)
    {

    });


}


void AdminCard::on_listWidget_2_itemDoubleClicked(QListWidgetItem *item)
{
    ui->lineEdit->setText(item->text());
}

void AdminCard::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    ui->lineEdit->setText(item->text());
}
