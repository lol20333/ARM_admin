#ifndef TEACHER_H
#define TEACHER_H

#include <QWidget>

namespace Ui {
class Teacher;
}

class Teacher : public QWidget
{
    Q_OBJECT

public:
    explicit Teacher(QWidget *parent,int rowid,int groupid);
    ~Teacher();
    int row_id;

private slots:
    void on_pushButton_clicked();

private:
    Ui::Teacher *ui;
};

#endif // TEACHER_H
