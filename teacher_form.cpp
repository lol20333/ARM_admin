#include "teacher_form.h"
#include "ui_teacher_form.h"
#include "admincard.h"
#include "ui_admincard.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newgroup.h"
#include "gruopwindow.h"
#include "loginserver.h"
#include "stuff/NetworkManager.h"
#include "timetable.h"
#include "studentnotes.h"
#include "admintable.h"
#include "review.h"
#include <QMessageBox>

Teacher_Form::Teacher_Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Teacher_Form)
{
    ui->setupUi(this);
    setWindowTitle("Регистрация преподавателя");
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM teacher WHERE NOT EXISTS (SELECT user_id FROM user_registered where teacher.rowid=user_id AND user_role_id =1)"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem1=new QListWidgetItem();
            QString name = row.get("teacher_name").asString();
            QString suname = row.get("teacher_suname").asString();
            QString patronymic = row.get("teacher_patronymic").asString();
            ListItem1->setText(suname+ " "+ name + " "+ patronymic);
            ListItem1->setData(Qt::UserRole,row.get("rowid").asInt());


            ui->listWidget->addItem(ListItem1);
        }

    });
}

Teacher_Form::~Teacher_Form()
{
    delete ui;
}

void Teacher_Form::on_pushButton_clicked()
{
    QString ad = ui->comboBox->currentText();
    QString da = ui->lineEdit->text();
    QString ada = ui->lineEdit_2->text();
    QString user_role_id;
    if (ada == "" || da == ""){
        QMessageBox::warning(this, "Внимание","y");
    }
    else{
        if(ad == "Методист"){
            user_role_id = "1";
        }
        if(ad == "Преподаватель"){
            user_role_id = "2";
        }
        if(ad == "Администратор"){
            user_role_id = "3";
        }

        NetworkManager::send(Request(SQL_OPERATOR, "", {
            {"sql_operator", "INSERT INTO user_registered (user_name,user_role_id,user_passw,user_id ) VALUES ( '"
              + ui->lineEdit_2->text() + "'," + user_role_id + "," + ui->lineEdit->text() + "," +rowid +")"
             " RETURNING rowid"
            }

        }), [=](const Response& resp)
        {

        });
    }
}

void Teacher_Form::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    ui->lineEdit_2->setText(item->text());
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM teacher "}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            ListItem->setData(Qt::UserRole,row.get("rowid").asString());
            rowid = ui->listWidget->selectedItems()[0]->data(Qt::UserRole).toString();



        }

    });
}

void Teacher_Form::on_comboBox_currentIndexChanged(const QString &arg1)
{
    if(arg1 == "Методист"){
        NetworkManager::send(Request(SQL_QUERY_XML, "", {
            {"sql_select", "SELECT * FROM teacher WHERE NOT EXISTS (SELECT user_id FROM user_registered where teacher.rowid=user_id AND user_role_id =1)"}
        }), [=](const Response& resp)
        {
            ui->listWidget->clear();
            for (size_t i = 0; i < resp.getRowCount(); ++i) {
                Response::Row row = resp.getRow(i);

                QListWidgetItem *ListItem1=new QListWidgetItem();
                QString name = row.get("teacher_name").asString();
                QString suname = row.get("teacher_suname").asString();
                QString patronymic = row.get("teacher_patronymic").asString();
                ListItem1->setText(suname+ " "+ name + " "+ patronymic);
                ListItem1->setData(Qt::UserRole,row.get("rowid").asInt());


                ui->listWidget->addItem(ListItem1);
            }

        });
      }
    if(arg1 == "Администратор"){
        NetworkManager::send(Request(SQL_QUERY_XML, "", {
            {"sql_select", "SELECT * FROM teacher WHERE NOT EXISTS (SELECT user_id FROM user_registered where teacher.rowid=user_id AND user_role_id = 3 )"}
        }), [=](const Response& resp)
        {
            ui->listWidget->clear();
            for (size_t i = 0; i < resp.getRowCount(); ++i) {
                Response::Row row = resp.getRow(i);

                QListWidgetItem *ListItem1=new QListWidgetItem();
                QString name = row.get("teacher_name").asString();
                QString suname = row.get("teacher_suname").asString();
                QString patronymic = row.get("teacher_patronymic").asString();
                ListItem1->setText(suname+ " "+ name + " "+ patronymic);
                ListItem1->setData(Qt::UserRole,row.get("rowid").asInt());


                ui->listWidget->addItem(ListItem1);
            }

        });
      }
    if(arg1 == "Преподаватель"){
        NetworkManager::send(Request(SQL_QUERY_XML, "", {
            {"sql_select", "SELECT * FROM teacher WHERE NOT EXISTS (SELECT user_id FROM user_registered where teacher.rowid=user_id AND user_role_id = 2)"}
        }), [=](const Response& resp)
        {
            ui->listWidget->clear();
            for (size_t i = 0; i < resp.getRowCount(); ++i) {
                Response::Row row = resp.getRow(i);

                QListWidgetItem *ListItem1=new QListWidgetItem();
                QString name = row.get("teacher_name").asString();
                QString suname = row.get("teacher_suname").asString();
                QString patronymic = row.get("teacher_patronymic").asString();
                ListItem1->setText(suname+ " "+ name + " "+ patronymic);
                ListItem1->setData(Qt::UserRole,row.get("rowid").asInt());


                ui->listWidget->addItem(ListItem1);
            }

        });
      }
}
