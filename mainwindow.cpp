#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newgroup.h"
#include "gruopwindow.h"
#include "loginserver.h"
#include "stuff/NetworkManager.h"
#include "timetable.h"
#include "studentnotes.h"
#include "admintable.h"
#include "review.h"
#include "teacher_form.h"
#include "student_form.h"
#include "teacher_card.h"
#include "group_student_registr.h"
#include "add_predmet.h"
#include "ui_add_predmet.h"
#include <QMessageBox>
#include <QProcess>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setWindowTitle("Основное окно");
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM group_student ORDER BY group_name ASC"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString groupName = row.get("group_name").asString();
            ListItem->setText(groupName);
            ListItem->setData(Qt::UserRole,row.get("rowid").asInt());


            ui->listWidget->addItem(ListItem);
        }

    });
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM kafedra"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem1=new QListWidgetItem();
            QString name = row.get("kafedra_name").asString();
            ListItem1->setText(name);
            ListItem1->setData(Qt::UserRole,row.get("rowid").asInt());


            ui->listWidget_2->addItem(ListItem1);
        }

    });

}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_clicked()//событие нажатия на кнопку "добавть группу"
{
    NewGroup *group = new NewGroup;
    group->show();
    connect(group,SIGNAL(onSelect(QString)), this, SLOT(onSelectNewgroup(QString)));
}

void MainWindow::on_action_triggered()//событие нажатия на кнопку "добавть группу" из верхнего меню
{
    on_pushButton_clicked();
}
void MainWindow::onSelectNewgroup(QString lblList)//слот добавления текста поля группы в список групп
{
    if(lblList == "")
    {
        QMessageBox::warning(this, "Внимание","Введите название группы");
        return;
    }
    NetworkManager::send(Request(SQL_OPERATOR, "", {
        {"sql_operator", "INSERT INTO group_student (group_name, group_id,leader_id ) VALUES ( '" + lblList + "', 0,0) RETURNING rowid"}
    }), [=](const Response& resp)
    {

            Response::Row row = resp.getRow(0);

            QListWidgetItem *ListItem=new QListWidgetItem();
            ListItem->setText(lblList);
            ListItem->setData(Qt::UserRole,row.get("rowid").asInt());


            ui->listWidget->addItem(ListItem);



    });

}

void MainWindow::on_listWidget_doubleClicked(const QModelIndex &index)//вызывается при двойном клике на группу
{

    auto rowid = ui->listWidget->selectedItems()[0]->data(Qt::UserRole).toInt();
    auto groupName = ui->listWidget->selectedItems()[0]->data(Qt::DisplayRole).toString();

    gruopwindow *groupstudents = new gruopwindow(rowid, groupName);

    groupstudents->show();

}

void MainWindow::on_pushButton_2_clicked() //Кнопка удаления группы
{



    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    foreach(QListWidgetItem * item, items)
    {
        QString rowid = ui->listWidget->selectedItems()[0]->data(Qt::UserRole).toString();
        NetworkManager::send(Request(SQL_OPERATOR, "", {
            {"sql_operator", "DELETE FROM group_student WHERE rowid='" + rowid + "';"}
        }), [=](const Response& resp)
        {

        });
        delete ui->listWidget->takeItem(ui->listWidget->row(item));
    }
}

void MainWindow::on_pushButton_3_clicked()// Кнопка вызова расписания потока
{
    Timetable *timeteble = new Timetable;
    timeteble->show();
}

void MainWindow::on_pushButton_4_clicked() // вызвать список заметок студентов
{
    StudentNotes *student_notes = new StudentNotes();
    student_notes->show();
}

void MainWindow::on_action_3_triggered()
{
    Review *review = new Review;
    review->show();
}

void MainWindow::on_pushButton_5_clicked()//открыть меню с правами
{
    AdminTable *admin_table = new AdminTable;
    admin_table->show();
}

void MainWindow::on_pushButton_6_clicked() // регистрация преподавателя
{
    Teacher_Form *teacher_form = new Teacher_Form;
    teacher_form->show();
}

void MainWindow::on_pushButton_7_clicked()
{
    Student_Form *student_form = new Student_Form;
    student_form->show();
}

void MainWindow::on_listWidget_2_itemDoubleClicked(QListWidgetItem *item)
{
    auto rowid = ui->listWidget_2->selectedItems()[0]->data(Qt::UserRole).toInt();
    auto groupName = ui->listWidget_2->selectedItems()[0]->data(Qt::DisplayRole).toString();
    Teacher_card *teacher_card = new Teacher_card(0,rowid,groupName);
    teacher_card->show();
}

void MainWindow::on_pushButton_8_clicked()
{

}

void MainWindow::on_pushButton_10_clicked()
{
    Group_student_registr *group_student = new Group_student_registr;
    group_student->show();
}

void MainWindow::on_pushButton_11_clicked()
{
    Add_predmet *add_predmet = new Add_predmet;
    add_predmet->show();
}

void MainWindow::on_action_4_triggered()
{
    QProcess *HelpSystem = new QProcess(this);
    HelpSystem ->setWorkingDirectory ("C:\\Users\\diplom\\Documents\\ARM_Admin\\HelpSystem");

    HelpSystem->start("C:\\Users\\diplom\\Documents\\ARM_Admin\\HelpSystem\\HelpSystem.exe");
}



















