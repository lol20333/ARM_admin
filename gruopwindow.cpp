#include "gruopwindow.h"
#include "studentcard.h"
#include "stuff/NetworkManager.h"
#include <QDebug>
#include "studentattedance.h"
#include <QDate>

gruopwindow::gruopwindow(int rowid, QString groupName) : QWidget(nullptr)
{
    group_id = rowid;
    name = new QLabel("Введите Имя студента: ");
    suname = new QLabel("Введите Фамилию студента: ");
    patronymic = new QLabel("Введите Отчество студента: ");
    studentnumber = new QLabel("Введите номер студенческого: ");
    text = new QLineEdit();
    text1 = new QLineEdit();
    text2 = new QLineEdit();
    text3 = new QLineEdit();
    btn1 = new QPushButton("Добавить студента");
    btnDel = new QPushButton("Удалить студента");
    btnAttendance = new QPushButton("Открыть посещаемость группы");
    lwidget = new QListWidget();
    QDateTime dat;
    QString gf;
       dat =QDateTime::currentDateTime();
       gf = dat.toString("yyyy");
       gf.toInt();


    QVBoxLayout *groupwindowV = new QVBoxLayout;

    groupwindowV->addWidget(suname);
    groupwindowV->addWidget(text1);
    groupwindowV->addWidget(name);
    groupwindowV->addWidget(text);
    groupwindowV->addWidget(patronymic);
    groupwindowV->addWidget(text2);
    groupwindowV->addWidget(studentnumber);
    groupwindowV->addWidget(text3);
    groupwindowV->addWidget(btn1);
    groupwindowV->addWidget(btnDel);
    groupwindowV->addWidget(btnAttendance);

    QHBoxLayout *groupwindowH = new QHBoxLayout;
    groupwindowH->addWidget(lwidget);

    QHBoxLayout *main = new QHBoxLayout;
    main->addLayout(groupwindowV);
    main->addLayout(groupwindowH);

    connect(btn1,SIGNAL(clicked()), this, SLOT(StudentCLick()));
    connect(btnDel,SIGNAL(clicked()), this, SLOT(StudentRemove()));
    connect(btnAttendance,SIGNAL(clicked()), this, SLOT(StudentAttendanceBtn()));
    connect(lwidget,SIGNAL(doubleClicked(QModelIndex)), this, SLOT(clickStudentCard()));

    setGeometry(QRect(100,100,900,120));
    int a = gf.toInt() - (groupName.right(2).toInt()+2000);
    qDebug()<< a;
    setWindowTitle("Группа: " +groupName + "       Курс: "+ QString::number(a));
    setLayout(main);

    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM student WHERE group_id = " + QString::number(rowid) + "ORDER BY student_name ASC"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString name = row.get("student_name").asString();
            QString suname = row.get("student_suname").asString();
            QString patronymic = row.get("student_patronymic").asString();
            QString studentnumber = row.get("student_number").asString();
            ListItem->setText(suname+ " "+ name + " "+ patronymic + "             " + studentnumber);
            ListItem->setData(Qt::UserRole,row.get("rowid").asInt());


            lwidget->addItem(ListItem);
        }

    });
}
void gruopwindow::StudentCLick()
{
    NetworkManager::send(Request(SQL_OPERATOR, "", {
        {"sql_operator", "INSERT INTO student (student_name, student_suname,student_patronymic,student_number,group_id ) VALUES ( '"
          + text->text() + "',"
         "'" + text1->text() + "',"
         "'" + text2->text() + "',"
         "'" + text3->text() + "',"
         "" + QString::number(group_id)
         +") RETURNING rowid"
        }
    }), [=](const Response& resp)
    {
            Response::Row row = resp.getRow(0);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString textLbl = text1->text() + " " + text->text() + " " + text2->text() + "             " + text3->text();
            ListItem->setText(textLbl);
            ListItem->setData(Qt::UserRole,row.get("rowid").asInt());

            lwidget->addItem(ListItem);
    });
}
void gruopwindow::StudentRemove()
{
    //QMessageBox::warning(this, "Внимание","y");
    QList<QListWidgetItem*> items = lwidget->selectedItems();
    foreach(QListWidgetItem * item, items)
    {
        QString rowid = lwidget->selectedItems()[0]->data(Qt::UserRole).toString();
        NetworkManager::send(Request(SQL_OPERATOR, "", {
            {"sql_operator", "DELETE FROM student WHERE rowid='" + rowid + "';"}
        }), [=](const Response& resp)
        {
            delete lwidget->takeItem(lwidget->row(item));
        });

    }
}
void gruopwindow::onSelectNewstudent(QString lblList)//слот добавления текста поля группы в список групп
{
   // QMessageBox::warning(this, "Внимание",lblList);
    lwidget->addItem(lblList);
}
void gruopwindow::clickStudentCard()
{

    int rowid = lwidget->selectedItems()[0]->data(Qt::UserRole).toInt();
    int groupid = group_id;

    Studentcard *studCard = new Studentcard(rowid, groupid);
    studCard->show();
}
void gruopwindow::StudentAttendanceBtn()
{
    StudentAttedance *student_attedance = new StudentAttedance();
    student_attedance->show();
}
