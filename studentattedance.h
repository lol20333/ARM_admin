#ifndef STUDENTATTEDANCE_H
#define STUDENTATTEDANCE_H

#include <QWidget>

namespace Ui {
class StudentAttedance;
}

class StudentAttedance : public QWidget
{
    Q_OBJECT

public:
    explicit StudentAttedance(QWidget *parent = 0);
    ~StudentAttedance();

private:
    Ui::StudentAttedance *ui;

};

#endif // STUDENTATTEDANCE_H
