#-------------------------------------------------
#
# Project created by QtCreator 2018-03-18T10:17:04
#
#-------------------------------------------------

QT      += core gui widgets websockets xml sql
CONFIG	+= c++14
CONFIG  += qaxcontainer


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ARM_Admin
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0




FORMS += \
        mainwindow.ui \
    studentcard.ui \
    loginserver.ui \
    createaccount.ui \
    timetable.ui \
    studentnotes.ui \
    studentattedance.ui \
    review.ui \
    admintable.ui \
    admincard.ui \
    teacher_form.ui \
    student_form.ui \
    teacher_card.ui \
    teacher.ui \
    group_student_registr.ui \
    institut_card.ui \
    add_predmet.ui

HEADERS += \
    stuff/Async.h \
    stuff/Log.h \
    stuff/NetworkManager.h \
    stuff/Query.h \
    stuff/Request.h \
    stuff/Response.h \
    gruopwindow.h \
    mainwindow.h \
    newgroup.h \
    studentcard.h \
    loginserver.h \
    createaccount.h \
    timetable.h \
    docloader.h \
    studentnotes.h \
    studentattedance.h \
    review.h \
    admintable.h \
    admincard.h \
    teacher_form.h \
    student_form.h \
    teacher_card.h \
    teacher.h \
    group_student_registr.h \
    institut_card.h \
    add_predmet.h

SOURCES += \
    stuff/Async.cpp \
    stuff/Log.cpp \
    stuff/NetworkManager.cpp \
    stuff/Query.cpp \
    stuff/Request.cpp \
    stuff/Response.cpp \
    gruopwindow.cpp \
    main.cpp \
    mainwindow.cpp \
    newgroup.cpp \
    studentcard.cpp \
    loginserver.cpp \
    createaccount.cpp \
    timetable.cpp \
    docloader.cpp \
    studentnotes.cpp \
    studentattedance.cpp \
    review.cpp \
    admintable.cpp \
    admincard.cpp \
    teacher_form.cpp \
    student_form.cpp \
    teacher_card.cpp \
    teacher.cpp \
    group_student_registr.cpp \
    institut_card.cpp \
    add_predmet.cpp

DISTFILES += \
    ../build-ARM_Admin-Desktop_Qt_5_10_1_MinGW_32bit-Debug/log.txt \
    ../build-ARM_Admin-Desktop_Qt_5_10_1_MinGW_32bit-Debug/simple.qss
