const pageBaseFolder = "pages/";
var historyPointer = 0;
const historyPages = [];

const viewport = document.getElementById('viewport');
const contentLoader = document.getElementById('content-loader');
const btnHistoryBack = document.getElementById("btnHistoryBack");
const btnHistoryNext = document.getElementById("btnHistoryNext");
const navSearch = document.getElementById("navSearch");
const mainMenu = document.querySelector("#mainMenu");

function init() {

    navSearch.addEventListener("keyup", function(event) {
        event.preventDefault();
        if (event.keyCode === 13) {
            getPages(navSearch.value);
        }
     });

    getPages();
}

function getPages(query) {

    var xmlhttp = new XMLHttpRequest();
    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
            getPagesRespose(JSON.parse(xmlhttp.responseText));
        }
    };

    if(query)
        xmlhttp.open("GET", "api/pages?query=" + query, true);
    else
        xmlhttp.open("GET", "api/pages", true);

    xmlhttp.send();
}

function getPagesRespose(items) {

    mainMenu.innerHTML = '';

    for (var item of items) {

        var name = item.replace(/\.md$/, '');
        var menuItem = document.createElement("li");
        menuItem.innerHTML = "<a data-page='" + item + "'>" + name + "</a>";

        mainMenu.appendChild(menuItem);
        mainMenu.onclick = onClickLink;
    }

    openPage(items[0]);
}

function onClickLink(event) {
    openPage(event.srcElement.getAttribute("data-page"));
    event.stopPropagation();
}

function selectLink(page) {

    for (var link of document.querySelectorAll("[data-page]")) {
        if(link.getAttribute("data-page") == page)
            link.classList.add('select');
        else
            link.classList.remove('select');
    }
}

function openPage(page) {
    historyPages.push(page);
    historyPointer = historyPages.length - 1;
	btnHistoryUpdateState();

    openPageWithoutHistory(page);
}

function openPageWithoutHistory(page) {
    if(!page) return;

    var xmlhttp = new XMLHttpRequest();

    xmlhttp.onreadystatechange = function() {
        if (xmlhttp.readyState == XMLHttpRequest.DONE) {
           if (xmlhttp.status == 200) {
                viewport.innerHTML = marked(xmlhttp.responseText);
           }
           else {
                viewport.innerHTML = marked("## Error\n\n### Network error");
           }
        }
    };

    xmlhttp.open("GET", pageBaseFolder + page, true);
    xmlhttp.send();
    selectLink(page);
}

function historyBack() {
    if(historyPointer <= 0)
        return;

    historyPointer--;
    openPageWithoutHistory(historyPages[historyPointer]);
	btnHistoryUpdateState();
}

function historyNext() {
    if(historyPointer >= historyPages.length - 1)
        return;

    historyPointer++;
    openPageWithoutHistory(historyPages[historyPointer]);
	btnHistoryUpdateState();
}

function btnHistoryUpdateState() {
	
	if(historyPointer <= 0)
		btnHistoryBack.classList.add('disable');
	else
		btnHistoryBack.classList.remove('disable');
		
    if(historyPointer >= historyPages.length - 1)
		btnHistoryNext.classList.add('disable');
	else
		btnHistoryNext.classList.remove('disable');
}

init();
