#include "group_student_registr.h"
#include "ui_group_student_registr.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newgroup.h"
#include "gruopwindow.h"
#include "loginserver.h"
#include "stuff/NetworkManager.h"
#include "timetable.h"
#include "studentnotes.h"
#include "admintable.h"
#include "review.h"
#include "teacher_form.h"
#include "student_form.h"
#include "teacher_card.h"
#include "group_student_registr.h"
#include <QMessageBox>

Group_student_registr::Group_student_registr(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Group_student_registr)
{
    ui->setupUi(this);
    setWindowTitle("Регистрация группы студентов");
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM group_student ORDER BY group_name ASC"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString groupName = row.get("group_name").asString();
            ListItem->setText(groupName);
            ListItem->setData(Qt::UserRole,row.get("rowid").asInt());


            ui->listWidget->addItem(ListItem);
        }

    });
}

Group_student_registr::~Group_student_registr()
{
    delete ui;
}

void Group_student_registr::on_pushButton_clicked()
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    foreach(QListWidgetItem * item, items)
    {
        QString rowid = ui->listWidget->selectedItems()[0]->data(Qt::UserRole).toString();
        NetworkManager::send(Request(SQL_OPERATOR, "", {
            {"sql_operator", "INSERT INTO  user_registered (user_role_id, user_id, user_passw, user_name, user_passport)"
             " SELECT 4, rowid, '1', CONCAT(student_suname,' ', student_name, ' ', student_patronymic), '' FROM student WHERE group_id =" + rowid }
        }), [=](const Response& resp)
        {

        });

    }
}
