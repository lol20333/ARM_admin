#ifndef TIMETABLE_H
#define TIMETABLE_H

#include <QWidget>

namespace Ui {
class Timetable;
}

class Timetable : public QWidget
{
    Q_OBJECT

public:
    explicit Timetable(QWidget *parent = 0);
    ~Timetable();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Timetable *ui;
};

#endif // TIMETABLE_H
