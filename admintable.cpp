#include "admintable.h"
#include "ui_admintable.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newgroup.h"
#include "gruopwindow.h"
#include "loginserver.h"
#include "stuff/NetworkManager.h"
#include "timetable.h"
#include "studentnotes.h"
#include "admintable.h"
#include "review.h"
#include "admincard.h"
#include <QMessageBox>

AdminTable::AdminTable(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::AdminTable)
{
    ui->setupUi(this);
    setWindowTitle("Назначить права");

}

AdminTable::~AdminTable()
{
    delete ui;
}

void AdminTable::on_listWidget_doubleClicked(const QModelIndex &index)
{

}

void AdminTable::on_Metodist_Btn_clicked()
{
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM user_registered WHERE user_role_id = 1"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString groupName = row.get("user_name").asString();
            ListItem->setText(groupName);



            ui->listWidget->addItem(ListItem);
        }

    });
    ui->listWidget->clear();
}

void AdminTable::on_Teacher_Btn_clicked()
{
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM user_registered WHERE user_role_id = 2"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString groupName = row.get("user_name").asString();
            ListItem->setText(groupName);



            ui->listWidget->addItem(ListItem);
        }

    });
    ui->listWidget->clear();
}

void AdminTable::on_Admin_Btn_clicked()
{

    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM user_registered WHERE user_role_id = 3"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString groupName = row.get("user_name").asString();
            ListItem->setText(groupName);



            ui->listWidget->addItem(ListItem);
        }
    });
    ui->listWidget->clear();
}

void AdminTable::on_Student_Btn_clicked()
{
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM user_registered WHERE user_role_id = 4"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString groupName = row.get("user_name").asString();
            ListItem->setText(groupName);



            ui->listWidget->addItem(ListItem);
        }

    });
    ui->listWidget->clear();
}

void AdminTable::on_pushButton_clicked()
{
    AdminCard *admin_card = new AdminCard();
    admin_card->show();
}

void AdminTable::on_pushButton_2_clicked()
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    foreach(QListWidgetItem * item, items)
    {
        NetworkManager::send(Request(SQL_OPERATOR, "", {
            {"sql_operator", "DELETE FROM user_registered WHERE user_name='" + ui->listWidget->currentItem()->text() + "';"}
        }), [=](const Response& resp)
        {

        });
        delete ui->listWidget->takeItem(ui->listWidget->row(item));
    }
}
