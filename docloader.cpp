#include "docloader.h"
#include <QAxObject>
#include <QSqlDatabase>
#include <QSqlQuery>
#include <QDebug>

DocLoader::DocLoader()
{
    table_widget = new QTableWidget;

QAxObject* excel = new QAxObject("Excel.Application", 0);
QAxObject* workbooks = excel->querySubObject("Workbooks");
QAxObject* workbook = workbooks->querySubObject("Open(const QString&)", "C:\\Users\\RealityShift3\\Downloads\\test2.xlsx");
QAxObject* sheets = workbook->querySubObject("Worksheets");
QAxObject* sheet = sheets->querySubObject("Item(int)", 1);

sheets->property("Count").toInt();


QAxObject* used_Range = sheet->querySubObject("UsedRange");
QAxObject* rows = used_Range->querySubObject("Rows");
int countRows = rows->property("Count").toInt();

QAxObject* usedRange = sheet->querySubObject("UsedRange");
QAxObject* columns = usedRange->querySubObject("Columns");
int countCols = columns->property("Count").toInt();
table_widget->setRowCount(countRows);
table_widget->setColumnCount(countCols);
for ( int row = 0; row < countRows; row++ ){
    for ( int column = 0; column < countCols; column++ ){
        QAxObject* cell = sheet->querySubObject("Cells(int,int)", column + 1, row + 1);
        QVariant value = cell->property("Value");
        QTableWidgetItem* item = new QTableWidgetItem(value.toString());
        table_widget->setItem(row, column, item);
    }
}

workbook->dynamicCall("Close()");
excel->dynamicCall("Quit()");
}
