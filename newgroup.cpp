#include "newgroup.h"

NewGroup::NewGroup(QWidget *parent) : QDialog(parent)
{
    lbl = new QLabel("Введите название группы: ");
    text = new QLineEdit();
    btn1 = new QPushButton("Добавить группу");
    QVBoxLayout *groupwindow = new QVBoxLayout;
    setGeometry(QRect(100,100,300,100));

    groupwindow->addWidget(lbl);
    groupwindow->addWidget(text);
    groupwindow->addWidget(btn1);
    connect(btn1,SIGNAL(clicked()), this, SLOT(GroupClick()));
    setWindowTitle("Добавление группы");
    setLayout(groupwindow);
}
void NewGroup::GroupClick()
{
    QString textLbl = text->text();
  //  QMessageBox::warning(this, "Внимание",textLbl);
    emit onSelect(textLbl);
    close();
}

