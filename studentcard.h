#ifndef STUDENTCARD_H
#define STUDENTCARD_H

#include <QDialog>

namespace Ui {
class Studentcard;
}

class Studentcard : public QDialog
{
    Q_OBJECT

public:
    explicit Studentcard(int rowid,int groupid);

    ~Studentcard();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Studentcard *ui;
    int row_id;
    int test_group_id;


};

#endif // STUDENTCARD_H
