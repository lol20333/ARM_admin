#include "studentnotes.h"
#include "stuff/NetworkManager.h"
#include "ui_studentnotes.h"

StudentNotes::StudentNotes(QWidget *parent ) :
    QWidget(parent),
    ui(new Ui::StudentNotes)
{
    ui->setupUi(this);
    setWindowTitle("Заметки студентов");
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM student WHERE group_id = 14 ORDER BY student_name ASC"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString name = row.get("student_name").asString();
            QString suname = row.get("student_suname").asString();
            QString patronymic = row.get("student_patronymic").asString();
            QString studentnumber = row.get("student_number").asString();
            ListItem->setText(suname+ " "+ name + " "+ patronymic + "             " + studentnumber);
            ListItem->setData(Qt::UserRole,row.get("rowid").asInt());



            ui->listWidget->addItem(ListItem);
        }

    });
}

StudentNotes::~StudentNotes()
{
    delete ui;
}
