#include "timetable.h"
#include "ui_timetable.h"
#include "docloader.h"

Timetable::Timetable(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Timetable)
{
    ui->setupUi(this);
    setWindowTitle("Расписание");
}

Timetable::~Timetable()
{
    delete ui;
}

void Timetable::on_pushButton_clicked()
{
    DocLoader *docloader = new DocLoader;
    docloader->table_widget->show();
}
