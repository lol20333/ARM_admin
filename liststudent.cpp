#include "liststudent.h"

class ListStudentData : public QSharedData
{
public:

};

ListStudent::ListStudent(QWidget *parent) : QWidget(parent), data(new ListStudentData)
{

}

ListStudent::ListStudent(const ListStudent &rhs) : data(rhs.data)
{

}

ListStudent &ListStudent::operator=(const ListStudent &rhs)
{
    if (this != &rhs)
        data.operator=(rhs.data);
    return *this;
}

ListStudent::~ListStudent()
{

}
