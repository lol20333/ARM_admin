#ifndef STUDENT_FORM_H
#define STUDENT_FORM_H

#include <QWidget>
#include <QListWidget>

namespace Ui {
class Student_Form;
}

class Student_Form : public QWidget
{
    Q_OBJECT

public:
    explicit Student_Form(QWidget *parent = 0);
    ~Student_Form();
    QString rowid;
    QString rowid_1;

private slots:
    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_pushButton_clicked();

private:
    Ui::Student_Form *ui;
};

#endif // STUDENT_FORM_H
