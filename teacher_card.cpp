#include "ui_teacher_card.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newgroup.h"
#include "gruopwindow.h"
#include "loginserver.h"
#include "stuff/NetworkManager.h"
#include "timetable.h"
#include "studentnotes.h"
#include "admintable.h"
#include "review.h"
#include "teacher_form.h"
#include "student_form.h"
#include "teacher_card.h"
#include "teacher.h"
#include <QMessageBox>

Teacher_card::Teacher_card(QWidget *parent,int rowid, QString groupName) :
    QWidget(parent),
    ui(new Ui::Teacher_card)
{
    ui->setupUi(this);
    ui->comboBox->addItem("t");
    ui->comboBox->addItem("c");
    kafedra_id = rowid;
    setWindowTitle("Кафедра: " + groupName);
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM teacher WHERE kafedra_id = " + QString::number(rowid)}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString name = row.get("teacher_name").asString();
            QString suname = row.get("teacher_suname").asString();
            QString patronymic = row.get("teacher_patronymic").asString();
            QString teachernumber = row.get("teacher_number").asString();
            ListItem->setText(suname+ " "+ name + " "+ patronymic + "             " + teachernumber);
            ListItem->setData(Qt::UserRole,row.get("rowid").asInt());


            ui->listWidget->addItem(ListItem);
        }

    });
}

Teacher_card::~Teacher_card()
{
    delete ui;
}

void Teacher_card::on_pushButton_clicked()
{
    NetworkManager::send(Request(SQL_OPERATOR, "", {
        {"sql_operator", "INSERT INTO teacher (teacher_name, teacher_suname,teacher_patronymic,teacher_number,kafedra_id ) VALUES ( '"
          + ui->lineEdit_2->text() + "',"
         "'" + ui->lineEdit->text() + "',"
         "'" + ui->lineEdit_3->text() + "',"
         "'" + ui->lineEdit_4->text() + "',"
         "" + QString::number(kafedra_id)
         +") RETURNING rowid"
        }
    }), [=](const Response& resp)
    {
            Response::Row row = resp.getRow(0);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString textLbl = ui->lineEdit->text() + " " + ui->lineEdit_2->text() + " " + ui->lineEdit_3->text() + "             " + ui->lineEdit_4->text();
            ListItem->setText(textLbl);
            ListItem->setData(Qt::UserRole,row.get("rowid").asInt());

            ui->listWidget->addItem(ListItem);
    });
}

void Teacher_card::on_pushButton_2_clicked()
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    foreach(QListWidgetItem * item, items)
    {
        QString rowid = ui->listWidget->selectedItems()[0]->data(Qt::UserRole).toString();
        NetworkManager::send(Request(SQL_OPERATOR, "", {
            {"sql_operator", "DELETE FROM teacher WHERE rowid='" + rowid + "';"}
        }), [=](const Response& resp)
        {
            delete ui->listWidget->takeItem(ui->listWidget->row(item));
        });

    }
}

void Teacher_card::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    int rowid = ui->listWidget->selectedItems()[0]->data(Qt::UserRole).toInt();
    int groupid = kafedra_id;
    Teacher *teacher = new Teacher(0,rowid,groupid);
    teacher->show();
}
