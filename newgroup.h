#ifndef NEWGROUP_H
#define NEWGROUP_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QDialog>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QPushButton>
#include <QMessageBox>

class NewGroup : public QDialog
{
    Q_OBJECT
public:
    NewGroup(QWidget *parent=0);
private:
    QLabel *lbl;
    QLineEdit *text;
    QPushButton *btn1;

public slots:
    void GroupClick();
signals:
    void onSelect(QString str_name);//Выбор имени новой группе студентов
};

#endif // NEWGROUP_H
