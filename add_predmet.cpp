#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newgroup.h"
#include "gruopwindow.h"
#include "loginserver.h"
#include "stuff/NetworkManager.h"
#include "timetable.h"
#include "studentnotes.h"
#include "admintable.h"
#include "review.h"
#include "teacher_form.h"
#include "student_form.h"
#include "teacher_card.h"
#include "group_student_registr.h"
#include "add_predmet.h"
#include "ui_add_predmet.h"
#include <QMessageBox>


Add_predmet::Add_predmet(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Add_predmet)
{
    ui->setupUi(this);
    setWindowTitle("Добавление предметов");
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM predmet"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString predmetName = row.get("predmet_name").asString();
            ListItem->setText(predmetName);



            ui->listWidget->addItem(ListItem);
        }

    });
}

Add_predmet::~Add_predmet()
{
    delete ui;
}

void Add_predmet::on_pushButton_clicked()
{
    NetworkManager::send(Request(SQL_OPERATOR, "", {
        {"sql_operator", "INSERT INTO predmet (predmet_name) VALUES ('" + ui->lineEdit->text() +"') RETURNING rowid"
        }
    }), [=](const Response& resp)
    {

    });
}

void Add_predmet::on_pushButton_2_clicked()
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    foreach(QListWidgetItem * item, items)
    {
        QString rowid = ui->listWidget->selectedItems()[0]->data(Qt::UserRole).toString();
        NetworkManager::send(Request(SQL_OPERATOR, "", {
            {"sql_operator", "DELETE FROM predmet WHERE rowid='" + rowid + "';"}
        }), [=](const Response& resp)
        {

        });
        delete ui->listWidget->takeItem(ui->listWidget->row(item));
    }
}
