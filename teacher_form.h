#ifndef TEACHER_FORM_H
#define TEACHER_FORM_H

#include <QWidget>
#include <QListWidget>

namespace Ui {
class Teacher_Form;
}

class Teacher_Form : public QWidget
{
    Q_OBJECT

public:
    explicit Teacher_Form(QWidget *parent = 0);
    ~Teacher_Form();
    QString rowid;

private slots:
    void on_pushButton_clicked();

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

    void on_comboBox_currentIndexChanged(const QString &arg1);

private:
    Ui::Teacher_Form *ui;
};

#endif // TEACHER_FORM_H
