#include "studentattedance.h"
#include "ui_studentattedance.h"
#include <QTableWidget>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newgroup.h"
#include "gruopwindow.h"
#include "loginserver.h"
#include "stuff/NetworkManager.h"
#include "timetable.h"
#include "studentnotes.h"
#include "admintable.h"
#include "review.h"
#include <QMessageBox>

StudentAttedance::StudentAttedance(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StudentAttedance)
{
    ui->setupUi(this);
    setWindowTitle("Посещаемость");

}

StudentAttedance::~StudentAttedance()
{
    delete ui;
}
