#ifndef GRUOPWINDOW_H
#define GRUOPWINDOW_H

#include <QMainWindow>
#include <QObject>
#include <QWidget>
#include <QLabel>
#include <QDialog>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QPushButton>
#include <QMessageBox>
#include <QListWidget>
class gruopwindow : public QWidget
{
    Q_OBJECT
public:
     gruopwindow(int rowid, QString groupName);
private:
    int group_id;
    QLabel *name;
    QLabel *suname;
    QLabel *patronymic;
    QLabel *studentnumber;
    QLineEdit *text;
    QLineEdit *text1;
    QLineEdit *text2;
    QLineEdit *text3;
    QPushButton *btn1;
    QPushButton *btnDel;
    QPushButton *btnAttendance;
    QListWidget *lwidget;

signals:
    void onSelect(QString str_name);
public slots:
    void StudentCLick();
    void StudentRemove();
    void StudentAttendanceBtn();
    void onSelectNewstudent(QString);
    void clickStudentCard();
};

#endif // GRUOPWINDOW_H
