#ifndef INSTITUT_CARD_H
#define INSTITUT_CARD_H

#include <QWidget>

namespace Ui {
class Institut_card;
}

class Institut_card : public QWidget
{
    Q_OBJECT

public:
    explicit Institut_card(QWidget *parent = 0);
    ~Institut_card();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Institut_card *ui;
};

#endif // INSTITUT_CARD_H
