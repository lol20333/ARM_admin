#ifndef LISTSTUDENT_H
#define LISTSTUDENT_H

#include <QMainWindow>
#include <QObject>
#include <QSharedDataPointer>
#include <QWidget>

class ListStudentData;

class ListStudent : public QWidget
{
    Q_OBJECT
public:
    explicit ListStudent(QWidget *parent = nullptr);
    ListStudent(const ListStudent &);
    ListStudent &operator=(const ListStudent &);
    ~ListStudent();

signals:

public slots:

private:
    QSharedDataPointer<ListStudentData> data;
};

#endif // LISTSTUDENT_H
