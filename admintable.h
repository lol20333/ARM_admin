#ifndef ADMINTABLE_H
#define ADMINTABLE_H

#include <QWidget>

namespace Ui {
class AdminTable;
}

class AdminTable : public QWidget
{
    Q_OBJECT

public:
    explicit AdminTable(QWidget *parent = 0);
    ~AdminTable();

private slots:

    void on_listWidget_doubleClicked(const QModelIndex &index);

    void on_Metodist_Btn_clicked();

    void on_Teacher_Btn_clicked();

    void on_Admin_Btn_clicked();

    void on_Student_Btn_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::AdminTable *ui;
};

#endif // ADMINTABLE_H
