#include "student_form.h"
#include "ui_student_form.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newgroup.h"
#include "gruopwindow.h"
#include "loginserver.h"
#include "stuff/NetworkManager.h"
#include "timetable.h"
#include "studentnotes.h"
#include "admintable.h"
#include "review.h"
#include "teacher_form.h"
#include <QMessageBox>

Student_Form::Student_Form(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Student_Form)
{
    ui->setupUi(this);
    setWindowTitle("Добавление студентов");
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM student WHERE NOT EXISTS (SELECT user_id FROM user_registered where student.rowid=user_id AND user_role_id = 4)"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            QString name = row.get("student_name").asString();
            QString suname = row.get("student_suname").asString();
            QString patronymic = row.get("student_patronymic").asString();
            ListItem->setText(suname+ " "+ name + " "+ patronymic);
            ListItem->setData(Qt::UserRole,row.get("rowid").asInt());


            ui->listWidget->addItem(ListItem);
        }


    });
}

Student_Form::~Student_Form()
{
    delete ui;
}

void Student_Form::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    ui->lineEdit->setText(item->text());
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM student "}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem=new QListWidgetItem();
            ListItem->setData(Qt::UserRole,row.get("rowid").asString());
            rowid = ui->listWidget->selectedItems()[0]->data(Qt::UserRole).toString();



        }

    });
}

void Student_Form::on_pushButton_clicked()
{
    NetworkManager::send(Request(SQL_OPERATOR, "", {
        {"sql_operator", "INSERT INTO user_registered (user_name,user_role_id,user_passw, user_id ) VALUES ( '"
          + ui->lineEdit->text() + "'," + "4" + "," + ui->lineEdit_2->text() + "," + rowid +")"
         " RETURNING rowid"
        }

    }), [=](const Response& resp)
    {

    });
}
