#ifndef GROUP_STUDENT_REGISTR_H
#define GROUP_STUDENT_REGISTR_H

#include <QWidget>

namespace Ui {
class Group_student_registr;
}

class Group_student_registr : public QWidget
{
    Q_OBJECT

public:
    explicit Group_student_registr(QWidget *parent = 0);
    ~Group_student_registr();

private slots:
    void on_pushButton_clicked();

private:
    Ui::Group_student_registr *ui;
};

#endif // GROUP_STUDENT_REGISTR_H
