#include "teacher.h"
#include "ui_teacher.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newgroup.h"
#include "gruopwindow.h"
#include "loginserver.h"
#include "stuff/NetworkManager.h"
#include "timetable.h"
#include "studentnotes.h"
#include "admintable.h"
#include "review.h"
#include "teacher_form.h"
#include "student_form.h"
#include "teacher_card.h"
#include <QMessageBox>

Teacher::Teacher(QWidget *parent, int rowid, int groupid) :
    QWidget(parent),
    ui(new Ui::Teacher)
{
    ui->setupUi(this);
    row_id = rowid;
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM teacher WHERE rowid = " + QString::number(rowid)}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

//            QListWidgetItem *ListItem=new QListWidgetItem();
            QString name = row.get("teacher_name").asString();
            QString suname = row.get("teacher_suname").asString();
            QString patronymic = row.get("teacher_patronymic").asString();
            QString teachernumber = row.get("teacher_number").asString();
            ui->lineEdit->setText(suname);
            ui->lineEdit_2->setText(name);
            ui->lineEdit_3->setText(patronymic);
            ui->lineEdit_4->setText(teachernumber);


        }

    });
}

Teacher::~Teacher()
{
    delete ui;
}

void Teacher::on_pushButton_clicked()
{
    NetworkManager::send(Request(SQL_OPERATOR, "", {
        {"sql_operator", "UPDATE teacher SET teacher_name = '" + ui->lineEdit->text() +
         "',teacher_suname ='" + ui->lineEdit_2->text() +
         "' ,teacher_patronymic ='" + ui->lineEdit_3->text() +
         "' ,teacher_number ='" + ui->lineEdit_4->text() +
         "' WHERE rowid =" + QString::number(row_id) +" "

        }
    }), [=](const Response& resp)
    {
        close();
    });
}
