#include "institut_card.h"
#include "ui_institut_card.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newgroup.h"
#include "gruopwindow.h"
#include "loginserver.h"
#include "stuff/NetworkManager.h"
#include "timetable.h"
#include "studentnotes.h"
#include "admintable.h"
#include "review.h"
#include "teacher_form.h"
#include "student_form.h"
#include "teacher_card.h"
#include "group_student_registr.h"
#include <QMessageBox>

Institut_card::Institut_card(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Institut_card)
{
    ui->setupUi(this);
    setWindowTitle("Добавление институтов");
}

Institut_card::~Institut_card()
{
    delete ui;
}

void Institut_card::on_pushButton_clicked()
{
    NetworkManager::send(Request(SQL_OPERATOR, "", {
        {"sql_operator", "INSERT INTO instituts (institute_name) VALUES ('" + ui->lineEdit->text() +"') RETURNING rowid"
        }
    }), [=](const Response& resp)
    {

    });
}
