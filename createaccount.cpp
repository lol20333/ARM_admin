#include "createaccount.h"
#include "ui_createaccount.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "newgroup.h"
#include "gruopwindow.h"
#include "loginserver.h"
#include "stuff/NetworkManager.h"
#include "timetable.h"
#include "studentnotes.h"
#include "admintable.h"
#include "review.h"
#include "teacher_form.h"
#include "student_form.h"
#include "teacher_card.h"
#include "group_student_registr.h"
#include "institut_card.h"
#include "ui_institut_card.h"
#include <QMessageBox>

CreateAccount::CreateAccount(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CreateAccount)
{
    ui->setupUi(this);
    setWindowTitle("Карточка институтов");
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM instituts"}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

            QListWidgetItem *ListItem1=new QListWidgetItem();
            QString name = row.get("institute_name").asString();
            ListItem1->setText(name);
            ListItem1->setData(Qt::UserRole,row.get("rowid").asInt());


            ui->listWidget->addItem(ListItem1);
        }

    });
}

CreateAccount::~CreateAccount()
{
    delete ui;
}

void CreateAccount::on_listWidget_itemDoubleClicked(QListWidgetItem *item)
{
    MainWindow *w = new MainWindow;
    w->show();
    close();
}

void CreateAccount::on_pushButton_clicked()
{
    Institut_card *institut_card = new Institut_card;
    institut_card->show();
}

void CreateAccount::on_pushButton_2_clicked()
{
    QList<QListWidgetItem*> items = ui->listWidget->selectedItems();
    foreach(QListWidgetItem * item, items)
    {
        QString rowid = ui->listWidget->selectedItems()[0]->data(Qt::UserRole).toString();
        NetworkManager::send(Request(SQL_OPERATOR, "", {
            {"sql_operator", "DELETE FROM instituts WHERE rowid='" + rowid + "';"}
        }), [=](const Response& resp)
        {

        });
        delete ui->listWidget->takeItem(ui->listWidget->row(item));
    }
}
