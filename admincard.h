#ifndef ADMINCARD_H
#define ADMINCARD_H

#include <QWidget>
#include <QListWidgetItem>

namespace Ui {
class AdminCard;
}

class AdminCard : public QWidget
{
    Q_OBJECT

public:
    explicit AdminCard(QWidget *parent = 0);
    ~AdminCard();

private slots:
    void on_pushButton_clicked();


    void on_listWidget_2_itemDoubleClicked(QListWidgetItem *item);

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::AdminCard *ui;
};

#endif // ADMINCARD_H
