#ifndef TEACHER_CARD_H
#define TEACHER_CARD_H

#include <QWidget>
#include <QListWidget>

namespace Ui {
class Teacher_card;
}

class Teacher_card : public QWidget
{
    Q_OBJECT

public:
    explicit Teacher_card(QWidget *parent , int rowid, QString groupName);
    ~Teacher_card();
    int kafedra_id;

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_listWidget_itemDoubleClicked(QListWidgetItem *item);

private:
    Ui::Teacher_card *ui;
};

#endif // TEACHER_CARD_H
