#ifndef ADD_PREDMET_H
#define ADD_PREDMET_H

#include <QWidget>

namespace Ui {
class Add_predmet;
}

class Add_predmet : public QWidget
{
    Q_OBJECT

public:
    explicit Add_predmet(QWidget *parent = 0);
    ~Add_predmet();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Add_predmet *ui;
};

#endif // ADD_PREDMET_H
