#include "studentcard.h"
#include "ui_studentcard.h"
#include "stuff/NetworkManager.h"
#include <QListWidget>

Studentcard::Studentcard(int rowid, int gruopid) :
    QDialog(nullptr),
    ui(new Ui::Studentcard)
{
      row_id = rowid;

    ui->setupUi(this);
    setWindowTitle("Карта студента");
    NetworkManager::send(Request(SQL_QUERY_XML, "", {
        {"sql_select", "SELECT * FROM student WHERE rowid = " + QString::number(rowid)}
    }), [=](const Response& resp)
    {
        for (size_t i = 0; i < resp.getRowCount(); ++i) {
            Response::Row row = resp.getRow(i);

//            QListWidgetItem *ListItem=new QListWidgetItem();
            QString name = row.get("student_name").asString();
            QString suname = row.get("student_suname").asString();
            QString patronymic = row.get("student_patronymic").asString();
            QString studentnumber = row.get("student_number").asString();
            ui->suname->setText(suname);
            ui->name->setText(name);
            ui->patronymic->setText(patronymic);
            ui->student_number->setText(studentnumber);


        }

    });

}

Studentcard::~Studentcard()
{
    delete ui;
}

void Studentcard::on_pushButton_clicked()
{
    NetworkManager::send(Request(SQL_OPERATOR, "", {
        {"sql_operator", "UPDATE student SET student_name = '" + ui->name->text() +
         "',student_suname ='" + ui->suname->text() +
         "' ,student_patronymic ='" + ui->patronymic->text() +
         "' ,student_number ='" + ui->student_number->text() +
         "' WHERE rowid =" + QString::number(row_id) +" "

        }
    }), [=](const Response& resp)
    {
        close();
    });
}




void Studentcard::on_pushButton_2_clicked()
{
    NetworkManager::send(Request(SQL_OPERATOR, "", {
        {"sql_operator", "INSERT INTO student (student_name, student_suname,student_patronymic,student_number,group_id ) VALUES ( '"
          + ui->name->text() + "',"
         "'" + ui->suname->text() + "',"
         "'" + ui->patronymic->text() + "',"
         "'" + ui->student_number->text() + "',"
         "14) RETURNING rowid"
        }
    }), [=](const Response& resp)
    {

    });
}
