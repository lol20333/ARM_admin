#ifndef LOGINSERVER_H
#define LOGINSERVER_H

#include <QWidget>

namespace Ui {
class LoginServer;
}

class LoginServer : public QWidget
{
    Q_OBJECT

public:
    explicit LoginServer(QWidget *parent = 0);
    ~LoginServer();

private slots:
    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

private:
    Ui::LoginServer *ui;
};

#endif // LOGINSERVER_H
